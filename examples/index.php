<?php
use Gilak\FiloTherapy\Therapist;

include __DIR__ . "/../vendor/autoload.php";
include "helpers.php";

$therapist = Therapist::factory();
$therapist->text("names.txt")->readLineByLine(function ($line) {
    echo $line . "<br>";
});

$therapist->text("names.txt")->read(function ($content) {
    echo "this is our content : " . $content;
});

$therapist->text("names.txt")->line(1, function ($content) {
    echo "this is our content : " . $content;
});

$therapist->text("names.txt")->lines([1, 2], function ($line) {
    echo "this is our line : " . $line . "<br>";
});

///////////////////////////

//$therapist->text("newNames.txt")->writeFromScratch("hasan gilak hakim abadi");
//$therapist->text("newNames.txt")->writeMore("elayas");
//
//$therapist->text("newNames.txt")->writeAfter(1, "dobare !");
$therapist->text("newNames.txt")->removeALine(1);
$therapist->text("newNames.txt")->read(function ($content) {
    echo "this is our content : " . $content;
});

