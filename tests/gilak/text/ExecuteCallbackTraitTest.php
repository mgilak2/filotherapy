<?php

use Gilak\FiloTherapy\Text\ExecuteCallbackTrait;

class ExecuteCallback
{
    use ExecuteCallbackTrait;
}


class ExecuteCallbackTraitTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ExecuteCallback
     */
    protected $executor;

    protected $didExecute = false;

    protected $arguments;

    protected $line;

    protected $arrayMap;

    public function setUp()
    {
        $this->executor = new ExecuteCallback;
    }

    public function testCallbackShouldExecuteAClosure()
    {
        $this->executor->callback(function () {
            $this->didExecute = true;
        });

        $this->assertTrue($this->didExecute);
    }

    public function testCallbackWithArgsShouldExecuteAClosureWithItsParams()
    {
        $this->executor->callbackWithArgs(function ($name, $lastName) {
            $this->arguments = [$name, $lastName];
        }, ['hasan', 'gilak']);

        $this->assertEquals($this->arguments, ['hasan', 'gilak']);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}