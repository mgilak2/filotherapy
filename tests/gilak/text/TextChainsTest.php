<?php

use Gilak\FiloTherapy\Text\TextChains;
use Gilak\FiloTherapy\Text\TextFile;
use Mockery as m;

class TextChainsTest extends PHPUnit_Framework_TestCase
{
    protected $data;
    protected $chunks;
    protected $fileName = __DIR__ . "/names.txt";
    protected $chunkFileName = __DIR__ . "/chunks.txt";
    protected $passedLineToAClosure;
    protected $readChunks;
    /**
     * @var TextChains
     */
    protected $textChains;

    public function setUp()
    {
        $this->createAnInstanceOfText();
        $this->createNamesTxtFile();
        $this->createChunksTxtFile();
    }

    private function createAnInstanceOfText()
    {
        $this->textChains = new TextChains();
        $this->textChains->text($this->fileName);
    }

    private function createNamesTxtFile()
    {
        $handle = fopen($this->fileName, 'w');
        $this->data = ["hasan gilak", "ali gilak", "elyas gilak", "taghi gilak",
            "mamad gilak", "rezvane gilak", "zahra gilak", "hasan gilak"];
        fwrite($handle, implode("\n", $this->data));
        fclose($handle);
    }

    private function createChunksTxtFile()
    {
        $handle = fopen($this->chunkFileName, 'w');
        $this->chunks = [
            "[warnings-2015-8-9] warnings  start 1
saddaslkdjalk jdaslkj dasklj daskld
daslkd jaslkdj aslkj dal dja end 1",
            "[notice-2015-8-10] notice sadkaslkda kdalsdj kjdkjn kjhf hiqowjd",
            "[dangerous-2015-8-11] dangerous this message is pretty much dangerous for u",
            "[warnings-2015-8-14] hasan agha gilak"
        ];
        fwrite($handle, implode("\n", $this->chunks));
        fclose($handle);
    }

    public function testTextShouldCreateATextFileObject()
    {
        $this->assertInstanceOf(TextFile::class, $this->textChains->getFile());
    }

    public function testReadLineByLineShouldExpectAClosureToHandleAnArrayOfLines()
    {
        $this->textChains->readLineByLine(function ($line) {
            $this->passedLineToAClosure[] = $line;
        });

        $this->assertEquals($this->data, $this->passedLineToAClosure);
    }

    public function testLinesShouldExpectAClosureAndAnArrayOfLineNumbersToPassToClosureAsAnActualLine()
    {
        $this->textChains->lines([0, 1], function ($line) {
            $this->passedLineToAClosure[] = $line;
        });

        $this->assertEquals(["hasan gilak", "ali gilak"], $this->passedLineToAClosure);
    }

    public function testLineShouldExpectAClosureAndALineNumberToPassToAClosureAsAnActualLine()
    {
        $this->textChains->line(0, function ($line) {
            $this->passedLineToAClosure = $line;
        });

        $this->assertEquals('hasan gilak', $this->passedLineToAClosure);
    }

    public function testWriteFromScratchShouldWriteToAFile()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("write")->with("hasan gilak")->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->writeFromScratch("hasan gilak");
    }

    public function testWriteMore()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("writeMore")->with("hasan gilak")->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->writeMore("hasan gilak");
    }

    public function testWriteMoreToANewLine()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("writeMore")->with("hasan gilak" . "\n")->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->writeMoreToANewLine("hasan gilak");
    }

    public function testWriteAfter()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("writeAfter")->with(1, "hasan gilak")->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->writeAfter(1, "hasan gilak");
    }

    public function testRemoveALine()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("removeALine")->with(1)->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->removeALine(1);
    }

    public function testRemoveLines()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("removeLines")->with([1, 2])->once();
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->removeLines([1, 2]);
    }

    public function testReplaceALine()
    {
        $mockTextFile = m::mock(TextFile::class);
        $mockTextFile->shouldReceive("extractAgainAllLinesIntoArray")->once();
        $mockTextFile->shouldReceive("getLine")->with(0)->once()->andReturn("hasan gilak");
        $mockTextFile->shouldReceive("getAllLines")->once()->andReturn(["hasan gilak"]);
        $mockTextFile->shouldReceive("write")->with("elyas gilak")->once();

        $this->textChains->setFile($mockTextFile);
        $this->textChains->replaceALine(0, "elyas gilak");
    }

    public function testReplaceALineWithNoMocks()
    {
        $textChains = new TextChains();
        $textChains->text($this->fileName)->replaceALine(0, "elyas gilak");
        $this->data[0] = "elyas gilak";
        $this->assertEquals(implode("\n", $this->data), $this->textChains->getFile()->content());
    }

    public function testFilterShouldExpectACallbackAndTakeLinesInsideItsCallback()
    {
        $textChains = new TextChains();
        $lines = $textChains->text($this->fileName)->filter(function ($line) {
            if (strpos($line, 'hasan') !== false)
                return $line;
        });

        $this->assertEquals(["hasan gilak", "hasan gilak"], $lines);
    }

    public function testSearchForALine()
    {
        $this->textChains->searchForAline('elyas gilak');
    }

    public function testSearchForALineWithNoMocks()
    {
        $textChains = new TextChains();
        $lines = $textChains->text($this->fileName)->searchForAline("elyas gilak");
        $this->assertEquals([3], $lines);
    }

    public function testSearchForAWord()
    {
        $this->assertEquals([3], $this->textChains->searchForAWord('elyas'));
    }

    public function testReplaceWord()
    {
        $this->textChains->replaceWord("elyas", "zakhar");
        $newContent = str_replace("elyas", "zakhar", implode("\n", $this->data));
        $this->assertEquals($newContent, $this->textChains->getFile()->content());
    }

    public function testReplaceWordInline()
    {
        $this->textChains->replaceWordInline(0, "hasan", "mohammad hasan");
        $this->data[0] = "mohammad hasan gilak";
        $this->assertEquals(implode("\n", $this->data), $this->textChains->getFile()->content());
    }

    public function testReplaceWordInLines()
    {
        $this->textChains->replaceWordInlines([0, 1], "gilak", "gilak hakim abadi");
        $this->data[0] = "hasan gilak hakim abadi";
        $this->data[1] = "ali gilak hakim abadi";

        $this->assertEquals(implode("\n", $this->data), $this->textChains->getFile()->content());
    }

    public function testAllChunks()
    {
        $this->prepareTextChainForChunk();
        $this->assertEquals([
                "[warnings-2015-8-9]" => "warnings  start 1
saddaslkdjalk jdaslkj dasklj daskld
daslkd jaslkdj aslkj dal dja end 1",
                "[notice-2015-8-10]" => "notice sadkaslkda kdalsdj kjdkjn kjhf hiqowjd",
                "[dangerous-2015-8-11]" => "dangerous this message is pretty much dangerous for u",
                "[warnings-2015-8-14]" => "hasan agha gilak"
            ]
            , $this->textChains->allChunks());
    }

    private function prepareTextChainForChunk()
    {
        $this->textChains->text($this->chunkFileName);
        $this->textChains->pattern("/\[[a-zA-Z]*\-[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}\]/");
    }

    public function testAllDelimitersInFile()
    {
        $this->prepareTextChainForChunk();

        $this->assertEquals([
            "[warnings-2015-8-9]",
            "[notice-2015-8-10]",
            "[dangerous-2015-8-11]",
            "[warnings-2015-8-14]"
        ], $this->textChains->allDelimitersInFile());
    }

    public function testHowManyChunkItHas()
    {
        $this->prepareTextChainForChunk();

        $this->assertEquals(4, $this->textChains->howManyChunkItHas());
    }

    public function testReadChunkByChunk()
    {
        $this->prepareTextChainForChunk();

        $this->textChains->readChunkByChunk(function ($delimiter, $chunk) {
            $this->readChunks[$delimiter] = $chunk;
        });

        $this->assertEquals([
            "[warnings-2015-8-9]" => "warnings  start 1
saddaslkdjalk jdaslkj dasklj daskld
daslkd jaslkdj aslkj dal dja end 1",
            "[notice-2015-8-10]" => "notice sadkaslkda kdalsdj kjdkjn kjhf hiqowjd",
            "[dangerous-2015-8-11]" => "dangerous this message is pretty much dangerous for u",
            "[warnings-2015-8-14]" => "hasan agha gilak"
        ], $this->readChunks);
    }

    public function testRemoveAChunk()
    {
        $this->prepareTextChainForChunk();

        $this->textChains->removeAChunk("[warnings-2015-8-9]");

        $this->assertEquals(implode("\n", [
            "[notice-2015-8-10]" => "notice sadkaslkda kdalsdj kjdkjn kjhf hiqowjd",
            "[dangerous-2015-8-11]" => "dangerous this message is pretty much dangerous for u",
            "[warnings-2015-8-14]" => "hasan agha gilak"
        ]), $this->textChains->getFile()->content());
    }

    public function testReplaceAChunk()
    {
        $this->prepareTextChainForChunk();

        $this->textChains->replaceAChunk("[warnings-2015-8-9]", "[danger-2015-9-9]", "avaz show miomio");
        $this->assertEquals([
            "[danger-2015-9-9]" => "avaz show miomio",
            "[notice-2015-8-10]" => "notice sadkaslkda kdalsdj kjdkjn kjhf hiqowjd",
            "[dangerous-2015-8-11]" => "dangerous this message is pretty much dangerous for u",
            "[warnings-2015-8-14]" => "hasan agha gilak"
        ], $this->textChains->getChunks());
    }

    public function testChunkFilter()
    {
        $this->prepareTextChainForChunk();

        $chunks = $this->textChains->chunkFilter(function ($delimiter, $chunk) {
            if (strpos($delimiter, "warnings") !== false)
                return [$delimiter => $chunk];
        });

        $this->assertEquals([
            "[warnings-2015-8-9]" => "warnings  start 1
saddaslkdjalk jdaslkj dasklj daskld
daslkd jaslkdj aslkj dal dja end 1",
            "[warnings-2015-8-14]" => "hasan agha gilak"
        ], $chunks);
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->removeCreatedFiles();
        $this->calledAClosure = false;
        $this->checkForMocksBehavior();
    }

    private function removeCreatedFiles()
    {
        unlink($this->fileName);
        unlink($this->chunkFileName);
    }

    private function checkForMocksBehavior()
    {
        m::close();
    }
}