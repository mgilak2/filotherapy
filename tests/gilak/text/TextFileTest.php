<?php

use Gilak\FiloTherapy\Text\TextFile;
use Mockery as m;

class TextFileTest extends PHPUnit_Framework_TestCase
{
    protected $fileName = __DIR__ . '/names.txt';
    protected $data;

    /**
     * @var TextFile
     */
    protected $file;

    public function setUp()
    {
        $this->createNamesTxtFile();
        $this->file = new TextFile($this->fileName);
    }

    private function createNamesTxtFile()
    {
        $handle = fopen($this->fileName, 'w');
        $this->data = ["hasan gilak", "ali gilak", "elyas gilak", "taghi gilak",
            "mamad gilak", "rezvane gilak", "zahra gilak", "hasan gilak"];
        fwrite($handle, implode("\n", $this->data));
        fclose($handle);
    }

    public function testContent()
    {
        $this->assertEquals(implode("\n", $this->data), $this->file->content());
    }

    public function testRenewFileContent()
    {
        $this->assertEquals(implode("\n", $this->data), $this->file->content());
    }

    public function testExtractAgainAllLinesIntoArray()
    {
        $this->assertEquals($this->data, $this->file->extractAgainAllLinesIntoArray());
    }

    public function testExtractAllLinesIntoArray()
    {
        $this->assertEquals($this->data, $this->file->extractAllLinesIntoArray());
    }

    public function testLineShouldReturnALineInStringBaseOnLineNumber()
    {
        $this->assertEquals("hasan gilak", $this->file->line(0));
    }

    public function testLineShouldReturnFalseBaseOnOutOfBandLineNumber()
    {
        $this->assertFalse($this->file->line(100));
    }

    public function testWriteMore()
    {
        $this->file->writeMore("umz");
        $this->assertEquals(implode("\n", $this->data) . "umz\n", $this->file->content());
    }

    public function testWriteAfter()
    {
        $this->file->writeAfter(0, "appended text after line 1");
        $this->data[0] .= "\nappended text after line 1";
        $this->assertEquals(implode("\n", $this->data), $this->file->content());
    }

    public function testGetLineShouldReturnALine()
    {
        $this->assertEquals("hasan gilak", $this->file->getLine(0));
    }

    public function testGetLineShouldReturnFalse()
    {
        $this->assertFalse($this->file->getLine(100));
    }

    public function testGetAllLines()
    {
        $this->assertEquals($this->data, $this->file->getAllLines());
    }

    public function testWrite()
    {
        $this->file->write("hasan gilak");
        $this->assertEquals("hasan gilak", $this->file->content());
    }

    public function testRemoveALine()
    {
        $this->file->removeALine(0);
        unset($this->data[0]);
        $this->assertEquals(implode("\n", $this->data), $this->file->content());
    }

    public function testRemoveLines()
    {
        $this->file->removeLines([0, 1]);
        unset($this->data[0]);
        unset($this->data[1]);
        $this->assertEquals(implode("\n", $this->data), $this->file->content());
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->removeCreatedFiles();
        $this->checkForMocksBehavior();
    }

    private function removeCreatedFiles()
    {
        unlink(__DIR__ . "/names.txt");
    }

    private function checkForMocksBehavior()
    {
        m::close();
    }
}