<?php

use Gilak\FiloTherapy\Text\TextChains;
use Gilak\FiloTherapy\Therapist;

class TherapistTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Therapist
     */
    protected $therapist;

    public function setUp()
    {
        $this->therapist = Therapist::factory();
        fopen(__DIR__ . "/simple.txt", 'w');
    }

    public function testTextShouldReturnAnInstanceOfTextFile()
    {
        $this->assertInstanceOf(TextChains::class, $this->therapist->text(__DIR__ . "/simple.txt"));
    }

    /**
     * @expectedException Gilak\FiloTherapy\Text\Exceptions\FileNotExistException
     */
    public function testTextShouldThrowFileNotExistsException()
    {
        $this->therapist->text(__DIR__ . "/emails.txt");
    }

    /**
     * @assert (0) == NULL
     * @expectedException Gilak\FiloTherapy\Text\Exceptions\NoPermissionForReadingException
     */
    public function ignore_testTextShouldThrowNoPermissionForReadingException()
    {
        $this->markTestSkipped('for running this test you should create a diary.txt file with no read permission.');
        $this->therapist->text(__DIR__ . "/diary.txt");
    }

    /**
     * @assert (0) == NULL
     * @expectedException Gilak\FiloTherapy\Text\Exceptions\NoPermissionForWritingException
     */
    public function ignore_testTextShouldThrowNoPermissionForWritingException()
    {
        $this->markTestSkipped('for running this test you should create a notebook.txt with no write permission');
        $this->therapist->text(__DIR__ . "/notebook.txt");
    }

    public function tearDown()
    {
        parent::tearDown();
        unlink(__DIR__ . "/simple.txt");
    }
}