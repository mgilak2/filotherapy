# filotherapy
working with files especially with text are pretty much easy now. 
no more search for how to write to a file in php.
 
currently text format files are supported. I have plan for other file types too. 

audio files

video files

image files

### Installation

```
composer require gilak/filotherapy
```


### Usage
```php
$therapist->text("names.txt")->readLineByLine(function ($line) {
    echo $line . "<br>";
});

$therapist->text("names.txt")->read(function ($content) {
    echo "this is our content : " . $content;
});

$therapist->text("names.txt")->line(1, function ($content) {
    echo "this is our content : " . $content;
});

$therapist->text("names.txt")->lines([1, 2], function ($line) {
    echo "this is our line : " . $line . "<br>";
});
```

for more usage cases see examples and tests folders 
