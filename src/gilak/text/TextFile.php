<?php namespace Gilak\FiloTherapy\Text;

use Gilak\FiloTherapy\Text\Exceptions\FileNotExistException;
use Gilak\FiloTherapy\Text\Exceptions\NoPermissionForReadingException;
use Gilak\FiloTherapy\Text\Exceptions\NoPermissionForWritingException;

class TextFile
{
    protected $fileAddress;
    protected $content;
    protected $allLines;

    public function __construct($fileAddress)
    {
        $this->fileAddress = $fileAddress;
    }

    public function isSafeForOperations()
    {
        $this->isFileExists();
        $this->isReadable();
        $this->isWritable();
        return true;
    }

    private function isFileExists()
    {
        if (!file_exists($this->fileAddress))
            throw new FileNotExistException("there is no such a file : " . $this->fileAddress);
    }

    private function isReadable()
    {
        if (!is_readable($this->fileAddress))
            throw new
            NoPermissionForReadingException("you don`t have permission to read this file " . $this->fileAddress);
    }

    private function isWritable()
    {
        if (!is_writable($this->fileAddress))
            throw new
            NoPermissionForWritingException("you can`t write to this file " . $this->fileAddress);
    }

    public function getFileContent()
    {
        if (is_null($this->content))
            $this->content = $this->content();
        return $this->content;
    }

    public function content()
    {
        return file_get_contents($this->fileAddress);
    }

    public function renewFileContent()
    {
        $this->content = $this->content();
        return $this->content;
    }

    /**
     * @return array
     */
    public function extractAgainAllLinesIntoArray()
    {
        $this->allLines = $this->extractAllLinesIntoArray();
        return $this->allLines;
    }

    /**
     * @return array
     */
    public function extractAllLinesIntoArray()
    {
        $lines = file($this->fileAddress);
        $trimmedLines = [];
        foreach ($lines as $line)
            $trimmedLines[] = trim($line);

        return $trimmedLines;
    }

    public function line($lineNumber)
    {
        $lines = $this->extractAllLinesIntoArray();
        return (isset($lines[$lineNumber])) ? $lines[$lineNumber] : false;
    }

    public function writeMore($newContent)
    {
        file_put_contents($this->fileAddress, $newContent . PHP_EOL, FILE_APPEND);
    }

    public function writeAfter($lineNumber, $newContent)
    {
        if ($this->getLine($lineNumber))
            $this->allLines[$lineNumber] .= "\n" . $newContent;
        $this->write(implode("\n", $this->allLines));
    }

    public function getLine($lineNumber)
    {
        $this->getAllLines();
        if (isset($this->allLines[$lineNumber]))
            return $this->allLines[$lineNumber];
        return false;
    }

    public function getAllLines()
    {
        if (is_null($this->allLines))
            $this->allLines = $this->extractAllLinesIntoArray();
        return $this->allLines;
    }

    public function write($newContent)
    {
        file_put_contents($this->fileAddress, $newContent);
    }

    public function removeALine($lineNumber)
    {
        if ($this->getLine($lineNumber)) {
            unset($this->allLines[$lineNumber]);
            $this->write(implode("\n", $this->allLines));
        }
    }

    public function removeLines($lineNumbers)
    {
        foreach ($lineNumbers as $line_number)
            if ($this->getLine($line_number))
                unset($this->allLines[$line_number]);

        $this->write(implode("\n", $this->allLines));
    }
}