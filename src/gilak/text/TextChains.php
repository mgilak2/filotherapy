<?php namespace Gilak\FiloTherapy\Text;

use Closure;

class TextChains
{
    use ExecuteCallbackTrait;

    protected $pattern;
    protected $chunks;
    protected $filteredLines;
    protected $filterIndex = 0;
    /**
     * @var TextFile
     */
    protected $file;

    /**
     * @param $fileAddress
     * @return $this
     */
    public function text($fileAddress)
    {
        $this->file = new TextFile($fileAddress);
        return $this;
    }

    /**
     * @return TextFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function setFile(TextFile $file)
    {
        $this->file = $file;
        return $this;
    }

    public function readLineByLine(Closure $callback)
    {
        $lines = $this->file->extractAllLinesIntoArray();
        array_map($callback, $lines);
    }

    public function lines(array $lineNumbers = [], Closure $callback)
    {
        array_map(function ($lineNumber) use ($callback) {
            $this->line($lineNumber, $callback);
        }, $lineNumbers);
    }

    public function line($lineNumber, Closure $callback)
    {
        return $this
            ->callbackWithArgs($callback, [$this->file->line($lineNumber)]);
    }

    public function writeFromScratch($newContent)
    {
        $this->file->write($newContent);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    private function refreshAllLinesTextFileClassVariables()
    {
        $this->file->extractAgainAllLinesIntoArray();
    }

    public function writeMore($newContent)
    {
        $this->file->writeMore($newContent);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function writeMoreToANewLine($newContent)
    {
        $this->file->writeMore($newContent . "\n");
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function writeAfter($lineNumber, $newContent)
    {
        $this->file->writeAfter($lineNumber, $newContent);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function removeALine($lineNumber)
    {
        $this->file->removeALine($lineNumber);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function removeLines($lineNumbers)
    {
        $this->file->removeLines($lineNumbers);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function replaceWord($search, $replace)
    {
        $newContent = str_replace($search, $replace, $this->file->content());
        $this->file->write($newContent);
        $this->refreshAllLinesTextFileClassVariables();
        return $this;
    }

    public function filter(Closure $callback)
    {
        $this->filteredLines = [];
        array_map(function ($line) use ($callback) {
            if ($filtered = $this->callbackWithArgs($callback, [$line])) {
                $this->filteredLines[] = $filtered;
            }
        }, $this->file->getAllLines());
        return $this->filteredLines;
    }

    public function searchForAline($givenLine)
    {
        $this->findGivenLine($givenLine);
        return $this->filteredLines;
    }

    private function findGivenLine($givenLine)
    {
        $this->restoreFilteredVariables();
        array_map(function ($line) use ($givenLine) {
            if ($line == $givenLine)
                $this->filteredLines[] = $this->filterIndex;
            $this->filterIndex++;
        }, $this->file->getAllLines());
        return $this->filteredLines;
    }

    private function restoreFilteredVariables()
    {
        $this->filteredLines = [];
        $this->filterIndex = 1;
    }

    public function searchForAWord($word)
    {
        $allPositions = [];
        foreach ($this->file->getAllLines() as $index => $line)
            if (($pos = strpos($line, $word, 0)) !== false)
                $allPositions[] = $index + 1;

        return (count($allPositions) > 0) ? $allPositions : false;
    }

    public function replaceWordInLines(array $lineNumbers = [], $search, $replace)
    {
        array_map(function ($lineNumber) use ($search, $replace) {
            $this->replaceWordInline($lineNumber, $search, $replace);
        }, $lineNumbers);

        return $this;
    }

    public function replaceWordInline($lineNumber, $search, $replace)
    {
        if ($line = $this->file->getLine($lineNumber)) {
            $replacedLine = str_replace($search, $replace, $line);
            $this->replaceALine($lineNumber, $replacedLine);
        }

        return $this;
    }

    public function replaceALine($lineNumber, $line)
    {
        if ($this->file->getLine($lineNumber)) {
            $lines = $this->file->getAllLines();
            $lines[$lineNumber] = $line;
            $this->file->write(implode("\n", $lines));
            $this->refreshAllLinesTextFileClassVariables();
        }
    }

    public function getChunks()
    {
        return $this->chunks;
    }

    public function readChunkByChunk(Closure $callback)
    {
        foreach ($this->allChunks() as $delimiter => $chunk)
            $this->callbackWithArgs($callback, [$delimiter, $chunk]);
    }

    public function allChunks()
    {
        if (is_null($this->chunks))
            $this->chunks = $this->extractAllChunks();
        return $this->chunks;
    }

    private function extractAllChunks()
    {
        $matched = $this->matchPattern();
        if ($this->doesPatternMatched($matched)) {
            $chunks = $this->resolveMultiLineChunks();
            $delimiters = $this->allDelimitersInFile();
            return array_combine($delimiters, $chunks);
        }

        return [];
    }

    private function matchPattern()
    {
        $didFindAny = preg_match_all($this->pattern, $this->file->content(), $delimiters);
        if ($didFindAny)
            return $delimiters;
        return [];
    }

    private function doesPatternMatched($matched)
    {
        if (count($matched) > 0)
            return true;
        return false;
    }

    private function resolveMultiLineChunks()
    {
        $chunks = preg_split($this->pattern, $this->file->content());
        $chunks = $this->leftShift($chunks);
        return $this->sanitizeSpecialCharacters($chunks);
    }

    private function leftShift($chunks)
    {
        $chunks = array_reverse($chunks);
        array_pop($chunks);
        $chunks = array_reverse($chunks);
        return $chunks;
    }

    private function sanitizeSpecialCharacters($chunks)
    {
        $sanitizedChunks = [];
        foreach ($chunks as $chunk)
            $sanitizedChunks[] = trim($chunk);

        return $sanitizedChunks;
    }

    public function allDelimitersInFile()
    {
        preg_match_all($this->pattern,
            $this->file->content(), $delimiters);

        return array_pop($delimiters);
    }

    public function howManyChunkItHas()
    {
        preg_match_all($this->pattern, $this->file->content(), $chunks);
        return count(array_pop($chunks));
    }

    /**
     * your chunk text begins with what ?
     * /(\[[a-z]+\-[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}\])/i
     * @param string $pattern
     * @return $this
     */
    public function pattern($pattern = "//")
    {
        $this->pattern = $pattern;
        return $this;
    }

    public function read(Closure $callback)
    {
        $this->callbackWithArgs($callback, [$this->file->content()]);
        return $this;
    }

    public function removeAChunk($delimiter)
    {
        if ($this->getChunk($delimiter)) {
            unset($this->chunks[$delimiter]);
            $this->file->write(implode("\n", $this->chunks));
            $this->refreshAllLinesTextFileClassVariables();
        }
    }

    public function getChunk($delimiter)
    {
        $chunks = $this->allChunks();
        if (isset($chunks[$delimiter]))
            return $chunks[$delimiter];
        return false;
    }

    public function replaceAChunk($oldDelimiter, $newDelimiter, $newChunk)
    {
        if ($this->getChunk($oldDelimiter)) {
            $index = $this->delimiterIndex($oldDelimiter);
            $this->chunks = $this->insertInBetween([$newDelimiter => $newChunk], $index + 1);
            unset($this->chunks[$oldDelimiter]);
        }
    }

    private function delimiterIndex($delimiter)
    {
        $index = -1;
        foreach ($this->allChunks() as $iteratedDelimiter => $chunk) {
            $index += 1;
            if ($delimiter == $iteratedDelimiter)
                return $index;
        }
        return -1;
    }

    private function insertInBetween($values, $offset)
    {
        return array_slice($this->chunks, 0, $offset, true)
        + $values
        + array_slice($this->chunks, $offset, NULL, true);
    }

    public function chunkFilter(Closure $callback)
    {
        $chunks = [];
        foreach ($this->allChunks() as $delimiter => $chunk) {
            $chunk = $this->callbackWithArgs($callback, [$delimiter, $chunk]);
            if (!is_null($chunk))
                $chunks = $chunks + $chunk;
        }

        return $chunks;
    }
}