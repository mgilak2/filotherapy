<?php

namespace Gilak\FiloTherapy\Text\Exceptions;

class NoPermissionForReadingException extends \Exception {}