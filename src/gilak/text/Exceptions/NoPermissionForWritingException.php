<?php

namespace Gilak\FiloTherapy\Text\Exceptions;

class NoPermissionForWritingException extends \Exception {}