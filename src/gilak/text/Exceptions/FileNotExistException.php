<?php

namespace Gilak\FiloTherapy\Text\Exceptions;

class FileNotExistException extends \Exception {}