<?php

namespace Gilak\FiloTherapy\Text;

use Closure;

trait ExecuteCallbackTrait
{
    public function callback(Closure $callback)
    {
        return call_user_func($callback);
    }

    public function callbackWithArgs(Closure $callback, array $arguments)
    {
        return call_user_func_array($callback, $arguments);
    }
}