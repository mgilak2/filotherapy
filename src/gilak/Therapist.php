<?php namespace Gilak\FiloTherapy;

use Gilak\FiloTherapy\Text\TextChains;
use Gilak\FiloTherapy\Text\TextFile;

class Therapist
{
    /**
     * @var TextChains
     */
    private $text;

    public function __construct(TextChains $text)
    {
        $this->text = $text;
    }

    public static function factory()
    {
        return new Therapist(new TextChains());
    }

    /**
     * @param $fileAddress
     * @return TextChains
     */
    public function text($fileAddress)
    {
        $text = $this->text->setFile(new TextFile($fileAddress));
        $text->getFile()->isSafeForOperations();
        return $text;
    }
}